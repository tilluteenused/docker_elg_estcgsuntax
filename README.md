# Container with a Constraint Grammar (CG) syntax analyzer for Estonian
[Constraint Grammar (CG) syntax analyzer for Estonian](https://github.com/EstSyntax/EstCG) container (docker) with
interface compliant with [ELG requirements](https://european-language-grid.readthedocs.io/en/release1.0.0/all/LTInternalAPI.html#).

## Contains  <a name="Contains"></a>

* [Constraint Grammar (CG) syntax analyzer for Estonian](https://github.com/EstSyntax/EstCG)
* [Filosoft morphological analyzer](https://github.com/Filosoft/vabamorf/blob/master/apps/cmdline/vmeta/)
* [EstNLTK](https://github.com/estnltk/estnltk) tokenizer 
* Container and interface code

## Preliminaries

* You should have software for making / using the container installed; see instructions on the [docker web site](https://docs.docker.com/).
* In case you want to compile the code or build the container yourself, you should have version control software installed; see instructions on the [git web site](https://git-scm.com/).

## Downloading image from Docker Hub

You may dowload a ready-made container from Docker Hub, using the Linux command line (Windows / Mac command lines are similar):

```commandline
docker pull tilluteenused/est_elg_cg3:2022.08.03
```
Next, continue to the section [Starting the container](#Starting_the_container).

## Making your own container

### 1. Downloading the source code

<!---
Lähtekood käib kokku 3 komponendist
1. ELG liides, veebiserver ja konteinerdamine
2. Kaili süntaks
3. FSi morf analüsaator
--->

The source code comes from two sources.

#### 1.1 Downloading [container and interface code](https://gitlab.com/tilluteenused/docker_elg_estcgsuntax) 

```cmdline
mkdir -p ~/gitlab-docker-elg
cd ~/gitlab-docker-elg
git clone https://gitlab.com/tilluteenused/docker_elg_estcgsuntax gitlab_docker_elg_estcgsuntax
```

The repo contains a compiled [morphological analyzer](https://github.com/Filosoft/vabamorf/blob/master/apps/cmdline/vmeta/) by Filosoft:

* **_vmeta_** - programme for morphological analysis
* **_et.dct_** - lexicon used by the analyzer 

In case you want to compile the program (**_vmeta_**) or change and re-assemble the lexicon (**_et.dct_**), follow the [instructions](https://github.com/Filosoft/vabamorf/blob/master/doc/make_programs_and_lexicons.md).

#### 1.2 Downloading [Constraint Grammar (CG) syntax analyzer for Estonian](https://github.com/EstSyntax/EstCG) 

```cmdline
cd ~/gitlab-docker-elg/gitlab_docker_elg_estcgsuntax
git clone https://github.com/EstSyntax/EstCG.git github_estcgsuntax
```

### 2. Building the container

```commandline
cd ~/gitlab-docker-elg/gitlab-docker-elg-synth
docker build -t tilluteenused/est_elg_cg3:2022.08.03 .
```

## Starting the container <a name="Starting_the_container"></a>

<!---

```cmdline
# Süntaksianalüsaatori käivitamine käsurealt
cd ~/gitlab-docker-elg/gitlab_docker_elg_estcgsuntax
./myparser.sh martjatoivo.txt
```
--->

```commandline
docker run -p 7000:7000 tilluteenused/est_elg_cg3:2022.08.03
```

One need not be in a specific directory to start the container.

Ctrl+C in a terminal window with a running container in it will terminate the container.

## Query json


```json
{
  "type":"text",
  "content": string, /* "The text of the request" */
}
```

## Response json

```json
{
  "response":
  {
    "type":"annotations",
    "annotations":
    {
      "sentence":
      [ /* array of sentences */
        {
          "start": number,     /* beginning of sentence (offset in characters) */
          "end": number        /* end of sentence (offset in characters) */
        }
      ],
      "token":
      [ /* array of all the tokens of all the sentences */
        {
          "start": number,     /* beginning of token (offset in characters) */
          "end": number,       /* end of token (offset in characters) */
          "features":
          { "token": string              /* token */
            "is_sent_start": boolean,    /* only if this is the beginning of sentence; otherwise missing */
            "is_sent_end": boolean,      /* only if this is the end of sentence; otherwise missing */
            "idx_bos": number,           /* index of the first token of this sentence (in the array of tokens) */
            "idx_current_token": number, /* index in this sentence */
            "idx_governor": number       /* index of the head of this token in this sentence */
            "morphosyntax":
            [ /* array of analyses */
              "lemma": string,              /* lemma */
              "features": string,           /* grammatical features */
              
            ]
          }
        }
      ]
    }
  }
}
```

## Usage example

Example sentence is _Koer leidis kondi._ (_The dog found a bone._)

```cmdline
curl -i --request POST --header "Content-Type: application/json" --data '{"type":"text","content":"Koer leidis kondi."}' localhost:7000/process
```

```json
HTTP/1.1 200 OK
Server: Werkzeug/2.2.1 Python/3.10.4
Date: Wed, 03 Aug 2022 18:15:43 GMT
Content-Type: application/json
Content-Length: 853
Connection: close

{"response":{"type":"annotations","annotations":{"sentence":[{"start":0,"end":18}],"token":[{"start":0,"end":4,"features":{"token":"Koer","is_sent_start":true,"idx_bos":0,"idx_current_token":0,"morphosyntax":[{"lemma":"Koer","features":"L0 S prop sg nom cap @SUBJ #1"}],"idx_governor":1}},{"start":5,"end":11,"features":{"token":"leidis","idx_bos":0,"idx_current_token":1,"morphosyntax":[{"lemma":"leid","features":"Lis V main indic impf ps3 sg ps af <FinV> <NGP-P> <0> @FMV #-1"}],"idx_governor":-1}},{"start":12,"end":17,"features":{"token":"kondi","idx_bos":0,"idx_current_token":2,"morphosyntax":[{"lemma":"kont","features":"L0 S com sg gen @OBJ #1"}],"idx_governor":1}},{"start":17,"end":18,"features":{"token":".","idx_bos":0,"idx_current_token":3,"morphosyntax":[{"lemma":".","features":"Z Fst CLB #3"}],"idx_governor":3,"is_sent_end":true}}]}}}
```

## Sponsors

The container development was sponsored by EU CEF project 
[Microservices at your service](https://www.lingsoft.fi/en/microservices-at-your-service-bridging-gap-between-nlp-research-and-industry)

## Authors

Authors of the container: Tarmo Vaino, Heiki-Jaan Kaalep

Authors of the contents of the container: see references at section [Contains](#Contains).
 


