#!/usr/bin/env python3
"""

Find sentence and token boundaries.

"""

import sys
from typing import Dict
from estnltk import Text
from estnltk.taggers import SentenceTokenizer

def estnltk_lausesta_text4elg(text_in: str) -> Dict:
    """Find sentences and tokens

    Args:
        text_in (str): plain text input

    Returns:
        Dict: sentence and token boundaries in ELG-annotation format
        {
            "sentence":
            [   /* lausete massiiv */
                {   "start": 0,      /* lause alguspositsioon */
                    "end": 17        /* lause lõpupositsioon */
                }
            ],
            "token":
            [   /* kõigi lausete kõigi sõnede massiiv */
                {
                    "start": number, /* sõne alguspositsioon */
                    "end": number,   /* sõne  lõpupositsioon */
                    "features":
                    {   "token": string /* sõne */
                    }
                }
            ]
        }
    """
    estnltk_text = Text(text_in)
    estnltk_text.tag_layer(['words'])
    SentenceTokenizer().tag(estnltk_text)
    sentences = []
    tokens = []
    for sentence in estnltk_text.sentences:
        for word in sentence:
            tokens.append({"start": word.start, "end": word.end,
                           "features": {"token": word.enclosing_text}})
        sentences.append({"start": sentence.start, "end": sentence.end})
        assert tokens[0]["start"] == sentences[0]["start"]
        assert tokens[-1]["end"] == sentences[-1]["end"]
    return {"sentence": sentences, "token": tokens}

def xml_text_out(sentences_and_tokens: Dict) -> str:
    """vmeta'le sobivalt lausestatud ja sõnestatud tekst faili

    Args:
        out_file (TextIO): Lausemärgenditega lause real tekst vmeta'le morfimiseks
        sentences_and_tokens (Dict): ELG formaadis lausestatud ja sõnestatud tekst
    """
    text = ''
    sentences = sentences_and_tokens["sentence"]
    tokens = sentences_and_tokens["token"]
    snt = tkn = 0
    while snt < len(sentences):
        assert tokens[tkn]["start"] == sentences[snt]["start"]
        text += '<s>'
        while tkn < len(tokens) and tokens[tkn]["end"] <= sentences[snt]["end"]:
            xml_token = tokens[tkn]["features"]["token"].replace('&', '&amp;').replace('<', '&lt;').replace('>', '&gt;').replace(' ', '<space/>')
            text += f' {xml_token}'
            tkn += 1
        text += ' </s>\n'
        snt += 1
    return text

if __name__ == '__main__':
    in_file = sys.stdin
    out_file = sys.stdout
    if len(sys.argv) == 3:
        if sys.argv[1] != '-':
            in_file = open(sys.argv[1], "r", encoding="utf-8")
        if sys.argv[2] != '-':
            out_file = open(sys.argv[2], "w", encoding="utf-8")
    elif len(sys.argv) != 1:
        print('süntaks:', sys.argv[0], '[{SISENDFAIL|-} {VÄLJUNDFAIL|-}]')
    out_file.write(xml_text_out(estnltk_lausesta_text4elg(in_file.read())))
