#!/usr/bin/env python3

"""

POOLELI

# command line script
./create_venv.sh
venv_elg_estnltk/bin/python3 ./elg_sdk_tokenizer.py --json='{"type":"text","content":"Koer leidis kondi."}'

docker build -t tilluteenused/est_elg_cg3:2022.08.03 .
docker login -u tilluteenused
docker push tilluteenused/est_elg_cg3:2022.08.03
docker run -p 7000:7000 tilluteenused/est_elg_cg3:2022.08.03
curl -i --request POST --header "Content-Type: application/json" --data '{"type":"text","content":"Koer leidis kondi."}' localhost:7000/process

"""

from typing import Dict
import sys
import json
import subprocess

from elg import FlaskService
from elg.model import AnnotationsResponse
from elg.model import TextRequest

import estnltk_tokenizer4elg


def run_est_cg_syntax(content: str) -> Dict:
    """Märgenda süntaks CG3 parseriga

    Args:
        content (str): Sisendtekst

    Returns:
        _type_: ELG'le vastava märgendusega json


    {
        "sentence":
        [   /* array of sentences */
            {
                "start": number,     /* beginning of sentence (offset in characters) */
                "end": number        /* end of sentence (offset in characters) */
            }
        ],
        "token":
        [   /* array of all the tokens of all the sentences */
            {
                "start": number,     /* beginning of token (offset in characters) */
                "end": number,       /* end of token (offset in characters) */
                "features":
                { 
                    "token": string              /* token */
                    "is_sent_start": boolean,    /* only if this is the beginning of sentence; otherwise missing */
                    "is_sent_end": boolean,      /* only if this is the end of sentence; otherwise missing */
                    "idx_bos": number,           /* index of the first token of this sentence (in the array of tokens) */
                    "idx_current_token": number, /* index in this sentence, first token in sentence is 0 */
                    "idx_governor": number       /* index of the head of this token in this sentence */
                    "morphosyntax":
                    [   /* array of analyses */
                        "lemma": string,              /* lemma */
                        "features": string,           /* grammatical features */  
                    ]
                }
            }
        ]
    }

    """
    resp = estnltk_tokenizer4elg.estnltk_lausesta_text4elg(content)
    txt = estnltk_tokenizer4elg.xml_text_out(resp)
    path = '.'
    proc = subprocess.Popen([f'{path}/parser4elg.sh'], text=True,
        stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.DEVNULL)
    #proc = subprocess.Popen([f'{path}/parser4elg.sh'], text=True,
    #    stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE)
    proc.stdin.write(txt)
    proc.stdin.close()
    lines = proc.stdout.read().splitlines()
    s = 0                           # sõnestajas: s-jooksev lause,
    sentences = resp["sentence"]    # sõnestajast: laused, otse väljundisse
    tokens = resp["token"]          # sõnestajast: sõnede massiiv, täiendame ja väljundisse
    l = 0                           # parseris: jooksev väljundirida
    t = 0                           # väljundis: t-jooksev sõne sõnede massiivis

    # lause algus: "is_sent_start" in resp["token"][t]["features"] == True  
    # lause lõpp: "is_sent_end" in resp["token"][t]["features"] == True
    while l < len(lines):
        assert sentences[s]["start"] == tokens[t]["start"]
        assert lines[l] == '"<s>"'
        l += 1
        # tokens[t].start, tokens[t].end, tokens[t]["features"]["token"] sõnestajast
        tokens[t]["features"]["is_sent_start"] = True
        lause_algus_idx = t
        while lines[l] != '"</s>"':
            line = lines[l].split('\t')
            tokens[t]["features"]["idx_bos"] = lause_algus_idx
            tokens[t]["features"]["idx_current_token"] = t-lause_algus_idx
            tokens[t]["features"]["morphosyntax"] = []

            for anal in line[1:]:
                anal = anal.split(' ')
                anal[len(anal)-1] = anal[len(anal)-1].split('->')
                governor = int(anal[len(anal)-1][1])-1
                tokens[t]["features"]["idx_governor"] = governor 
                if len(anal[0]) >= 3 and anal[0][0] == '"' and anal[0][len(anal[0])-1] == '"':
                    lemma = anal[0][1:len(anal[0])-1]
                else:
                    lemma = anal[0]
                tokens[t]["features"]["morphosyntax"].append({ 
                    "lemma": lemma,
                    "features":  ' '.join(anal[1:len(anal)-1]) + ' #' + str(governor)})
            if lines[l+1] == '"</s>"':
                tokens[t]["features"]["is_sent_end"] = True
            #json.dump(tokens[t], sys.stdout, indent=4)
            l += 1 # järgmine rida parseri väljundist
            t += 1 # järgmine sõne sõnestaja väljundist
        assert lines[l] == '"</s>"' # lause läbi
        s += 1
        l += 1
    return {'sentence': sentences, "token": tokens}


class EST_CG_SYNTAX(FlaskService):
    def process_text(self, request) -> AnnotationsResponse:
        """
        Find sentences and tokens
        :param content: {TextRequest} - input text in ELG compatible format
        :return: {AnnotationsResponse} - CG syntax in ELG compatible format
        """
        annotation_out = run_est_cg_syntax(request.content)
        return AnnotationsResponse(annotations=annotation_out)

flask_service = EST_CG_SYNTAX("EstNLTK tokenizer")
app = flask_service.app


def run_test(my_query_str: str) -> Dict:
    """
    Run as command line script
    :param my_query_str: input in json string
    """
    my_query = json.loads(my_query_str)
    service = EST_CG_SYNTAX("Estonian_Constraint_Grammar")
    request = TextRequest(content=my_query["content"])
    response = service.process_text(request)

    response_json_str = response.json(exclude_unset=True)  # exclude_none=True?
    response_json_json = json.loads(response_json_str)
    return response_json_json


def run_server() -> None:
    """Run as flask webserver"""
    app.run()

if __name__ == '__main__':
    import argparse
    argparser = argparse.ArgumentParser(allow_abbrev=False)
    argparser.add_argument('-j', '--json', type=str, help='ELG compatible json')
    argparser.add_argument('-t', '--textfile', type=str, help='plain text file')
    args = argparser.parse_args()
    if args.json is not None:
        json.dump(run_est_cg_syntax(json.loads(args.json)["content"]), sys.stdout, indent=4)
    elif args.textfile is not None:
        json.dump(run_est_cg_syntax(open(args.textfile, "r",
                            encoding="utf8").read()), sys.stdout, indent=4)
    else:
        run_server()
