# Eesti keele kitsenduste grammatika (CG) süntaksianalüsaatori konteiner
[Eesti keele kitsenduste grammatika (CG) süntaksianalüsaatorit](https://github.com/EstSyntax/EstCG) sisaldav tarkvara-konteiner (docker),
mille liides vastab [ELG nõuetele](https://european-language-grid.readthedocs.io/en/release1.0.0/all/LTInternalAPI.html#).

## Mida sisaldab <a name="Mida_sisaldab"></a>

* [Eesti keele kitsenduste grammatika (CG) süntaksianalüsaator](https://github.com/EstSyntax/EstCG)
* [Filosofti eesti keele morfoloogiline analüsaator](https://github.com/Filosoft/vabamorf/blob/master/apps/cmdline/vmeta/LOEMIND.md)
* [EstNLTK](https://github.com/estnltk/estnltk) lausestaja-sõnestaja <!--Seda ei paneks lingina GITLABi kuna tuleb paketihaldusest-->
* Konteineri ja liidesega seotud lähtekood

## Eeltingimused

* Peab olema paigaldatud tarkvara konteineri tegemiseks/kasutamiseks; juhised on [docker'i veebilehel](https://docs.docker.com/).
* Kui sooviks on lähtekoodi ise kompileerida või konteinerit kokku panna, siis peab olema paigaldatud versioonihaldustarkvara; juhised on [git'i veebilehel](https://git-scm.com/).


## Konteineri allalaadimine Docker Hub'ist

Valmis konteineri saab laadida alla Docker Hub'ist, kasutades Linux'i käsurida (Windows'i/Mac'i käsurida on analoogiline):

```commandline
docker pull tilluteenused/est_elg_cg3:2022.08.03
```

Seejärel saab jätkata osaga [Konteineri käivitamine](#Konteineri_käivitamine).

## Ise konteineri tegemine

### 1. Lähtekoodi allalaadimine

<!---
Lähtekood käib kokku 3 komponendist
1. ELG liides, veebiserver ja konteinerdamine
2. Kaili süntaks
3. FSi morf analüsaator
--->

Lähtekood tuleb laadida alla kahest kohast.

#### 1.1 [Konteineri ja liidesega seotud lähtekoodi](https://gitlab.com/tilluteenused/docker_elg_estcgsuntax) allalaadimine

```cmdline
mkdir -p ~/gitlab-docker-elg
cd ~/gitlab-docker-elg
git clone https://gitlab.com/tilluteenused/docker_elg_estcgsuntax gitlab_docker_elg_estcgsuntax
```

Repositoorium sisaldab kompileeritud [Filosofti morfoloogilist analüsaatorit](https://github.com/Filosoft/vabamorf/blob/master/apps/cmdline/vmeta/LOEMIND.md) ja andmefaile:

* **_vmeta_**  - morfoloogilise analüüsi programm.
* **_et.dct_** - programmi poolt kasutatav leksikon.

Kui soovite ise programmi (**_vmeta_**) kompileerida või leksikoni (**_et.dct_**) täiendada/muuta ja uuesti kokku panna, 
vaadake sellekohast [juhendit](https://github.com/Filosoft/vabamorf/blob/master/doc/programmid_ja_sonastikud.md).

#### 1.2 [Süntaksianalüsaatori lähtekoodi](https://github.com/EstSyntax/EstCG) allalaadimine

```cmdline
cd ~/gitlab-docker-elg/gitlab_docker_elg_estcgsuntax
git clone https://github.com/EstSyntax/EstCG.git github_estcgsuntax
```

### 2. Konteineri kokkupanemine

```commandline
cd ~/gitlab-docker-elg/gitlab-docker-elg-synth
docker build -t tilluteenused/est_elg_cg3:2022.08.03 .
```

## Konteineri käivitamine <a name="Konteineri_käivitamine"></a>

<!---

```cmdline
# Süntaksianalüsaatori käivitamine käsurealt
cd ~/gitlab-docker-elg/gitlab_docker_elg_estcgsuntax
./myparser.sh martjatoivo.txt
```
--->

```commandline
docker run -p 7000:7000 tilluteenused/est_elg_cg3:2022.08.03
```

Pole oluline, milline on jooksev kataloog terminaliaknas konteineri käivitamise hetkel.

Käivitatud konteineri töö lõpetab Ctrl+C selles terminaliaknas, kust konteiner käivitati.

## Päringu json-kuju

```json
{
  "type":"text",
  "content": string, /* Analüüsitav tekst. */
}
```

## Vastuse json-kuju

```json
{
  "response":
  {
    "type":"annotations",
    "annotations":
    {
      "sentence":
      [ /* lausete massiiv */
        {
          "start": number,      /* lause algus tekstis (märgijadas) */
          "end": number         /* lause lõpp tekstis (märgijadas) */
        }
      ],
      "token":
      [ /* kõigi lausete kõigi sõnede massiiv */
        {
          "start": number,  /* sõne algus tekstis (märgijadas) */
          "end": number,    /* sõne lõpp tekstis (märgijadas) */
          "features":
          { "token": string /* sõne */
            "is_sent_start": boolean,    /* kui on lauses esimene; muidu puudub */
            "is_sent_end": boolean,      /* kui on lauses viimane; muidu puudub */
            "idx_bos": number,           /* lause esimese sõne jrk nr sõnede massiivis */
            "idx_current_token": number, /* sõne jrk nr lauses */
            "idx_governor": number       /* ülemuseks oleva sõne jrk nr lauses */
            "morphosyntax":
            [ /* analüüsivariantide massiiv */
              {
                "lemma": string,            /* algvorm e. lemma */
                "features": string,         /* morfosüntaktiline info */
              }
            ]
          }
        }
      ]
    }
  }
}
```

## Kasutusnäide
Näitelause on _Koer leidis kondi._

```cmdline
curl -i --request POST --header "Content-Type: application/json" --data '{"type":"text","content":"Koer leidis kondi."}' localhost:7000/process
```

```json
HTTP/1.1 200 OK
Server: Werkzeug/2.2.1 Python/3.10.4
Date: Wed, 03 Aug 2022 18:15:43 GMT
Content-Type: application/json
Content-Length: 853
Connection: close

{"response":{"type":"annotations","annotations":{"sentence":[{"start":0,"end":18}],"token":[{"start":0,"end":4,"features":{"token":"Koer","is_sent_start":true,"idx_bos":0,"idx_current_token":0,"morphosyntax":[{"lemma":"Koer","features":"L0 S prop sg nom cap @SUBJ #1"}],"idx_governor":1}},{"start":5,"end":11,"features":{"token":"leidis","idx_bos":0,"idx_current_token":1,"morphosyntax":[{"lemma":"leid","features":"Lis V main indic impf ps3 sg ps af <FinV> <NGP-P> <0> @FMV #-1"}],"idx_governor":-1}},{"start":12,"end":17,"features":{"token":"kondi","idx_bos":0,"idx_current_token":2,"morphosyntax":[{"lemma":"kont","features":"L0 S com sg gen @OBJ #1"}],"idx_governor":1}},{"start":17,"end":18,"features":{"token":".","idx_bos":0,"idx_current_token":3,"morphosyntax":[{"lemma":".","features":"Z Fst CLB #3"}],"idx_governor":3,"is_sent_end":true}}]}}}

```

## Rahastus

Konteiner loodi EL projekti [Microservices at your service](https://www.lingsoft.fi/en/microservices-at-your-service-bridging-gap-between-nlp-research-and-industry) toel.

## Autorid

Konteineri autorid: Tarmo Vaino, Heiki-Jaan Kaalep

Konteineri sisu autoreid vt. jaotises [Mida sisaldab](#Mida_sisaldab) toodud viidetest.
 


