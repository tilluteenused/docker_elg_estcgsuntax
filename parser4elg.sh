#!/bin/bash
#set -euo pipefail
# set käsu kohta vt. https://pythonspeed.com/articles/shell-scripts/

# with_etana $1 -- Enam-vähem Kaili algne versioon:
#   Sisendfail (plain text): $1
#   Väljundfail: $1.cg3

# with_estnltk_and_vmeta -- ELG värgi seest kasutamiseks
#   Sisend: stdin: sõnestatud lausemärgenditega tekst, mis sobi vmeta --xml sisendiks
#   Väljund: stdout: veidi modifitseeritud Kalili skripti väljund
#   Näide:
#      echo "Mees peeti kinni. Silmad peas punnis." \
#      | venv/bin/python3 estnltk_tokenizer4elg.py  \
#      | ./parser4elg.sh \
#      2> /dev/null

RADA=github_estcgsuntax
RADATBL=github_estcgsuntax/vers2017
RADAMRF=.
RADADCT=.
PYTHON=${PYTHON:="venv/bin/python3"}

#RADA=${RADA:="~/Programmid/gitparser/november17"}
#RADATBL=${RADATBL:="/home/kaili/Programmid/gitparser/november17/tmorftrtabel.txt"}
#RADAMRF=${RADAMRF:="~/Programmid/vabamorf-master/apps/cmdline/project/unix"}
#RADADCT=${RADADCT:="~/Programmid/vabamorf-master/dct/binary/"}
#PYTHON=${PYTHON:="venv/bin/python3"}

with_etana()
{
    rm -f $1.cg3
    cat $1 \
    | $RADA/rlausestaja.pl \
    | $RADA/wr2json.pl \
    | $RADAMRF/etana analyze -lex $RADADCT/et.dct -guess \
    | sed -e "s/[\]n/\n/g" \
    | ./json2mrf.pl \
    | ./rtolkija.pl ${RADATBL} \
    | $RADA/pron17.pl \
    | $RADA/tcopyrem_kaili.pl \
    | $RADA/tkms2cg3.pl \
    | vislcg3 -g $RADA/preprocess.rul \
    | vislcg3 -o -g $RADA/clo.rul \
    | vislcg3 -o -g $RADA/morfyhe17.rul \
    | vislcg3 -o -g $RADA/PhVerbs17.rul \
    | vislcg3 -o -g $RADA/pindsyn17.rul \
    | vislcg3 -o -g $RADA/strukt.rul \
    > $1.cg3
}

with_estnltk_and_vmeta()
{
    $RADAMRF/vmeta --path . --xml --guess --guesspropnames --fs --dontaddphonetics \
    | sed 's/    /\n&/g' | sed 's#, //$# //#g' \
    | sed 's#<\(/*s\)>#____\1____#g' \
    | sed 's/&amp;/\&/g' | sed 's/&lt;/</g' | sed 's/&gt;/>/g' \
    | sed 's#____\(/*s\)____#<\1>#' \
    \
    | ./rtolkija.pl ${RADATBL} \
    | $RADA/pron17.pl \
    | $RADA/tcopyrem_kaili.pl \
    | $RADA/tkms2cg3.pl \
    | vislcg3    -g $RADA/preprocess.rul \
    | vislcg3 -o -g $RADA/clo.rul \
    | vislcg3 -o -g $RADA/morfyhe17.rul \
    | vislcg3 -o -g $RADA/PhVerbs17.rul \
    | vislcg3 -o -g $RADA/pindsyn17.rul \
    | vislcg3 -o -g $RADA/strukt.rul \
    \
    | tr -s '\n' \
    | sed 's/@/____ätmärgikesekene____/g' \
    | tr '\n' '@' | sed 's/@\t/\t/g' | tr '@' '\n' \
    | sed 's/____ätmärgikesekene____/@/g'

}

#with_etana $1
with_estnltk_and_vmeta $1

exit
